/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.ui.model;
import com.opensymphony.xwork2.ActionSupport;
import baby.shop.biz.Cart;
import baby.shop.entity.Product;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import java.util.Map;

/**
 *
 * @author tienh
 */
public class ViewCart extends ActionSupport {
    private Map<Product, Integer> products;
    private float total;

    @Override
    public String execute() throws Exception {
        //return super.execute(); //To change body of generated methods, choose Tools | Templates.
        Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
        if(cart == null) {
            return ERROR;
        }
        products = cart.getProducts();
        total = cart.getTotalPrice();
        return SUCCESS;
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<Product, Integer> products) {
        this.products = products;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
    
    

    
    
    
}
