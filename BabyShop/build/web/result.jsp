<%-- 
    Document   : result
    Created on : Aug 5, 2019, 7:55:03 PM
    Author     : tienh
--%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product List of search <s:property value="keyword"/></title>
    </head>
    <body>
        <h1>Product List of search <s:property value="keyword" /></h1>
        <table>
            <s:iterator value="products" var="product">
                <tr>
                    <td><s:property value="#product.name" /></td>
                    <td><s:property value="#product.price" /></td>
                    <td><s:property value="#product.description" /></td>
                    <td><a href="addToCart?newProductId=<s:property value="id"/>">Add to cart</a></td>
                </tr>
            </s:iterator>
                <a href="index.jsp">Back</a>
        </table>
    </body>
</html>
